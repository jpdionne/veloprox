#include "pins_arduino.h"

#define ECHO_PIN 2
#define TRIG_PIN 4 
#define ALARM_LED_PIN 13

void setup() {


  Serial.begin(9600);
  Serial.println("\nVeloprox v0.1");

  pinMode(ECHO_PIN, INPUT);
  pinMode(ALARM_LED_PIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);

  digitalWrite(TRIG_PIN, LOW); 
  digitalWrite(ALARM_LED_PIN, LOW);

}

volatile unsigned long echo_start = 0;
volatile unsigned long delta = 0;

void loop() {
  float meters;
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(20);
  digitalWrite(TRIG_PIN, LOW); 

  delta = pulseIn(ECHO_PIN, HIGH, 500000);
  Serial.println(delta);
  if (delta) {
    Serial.print(" Metres: ");
    meters = delta/1000000.0 * 340 / 2;
    Serial.println(meters, DEC);
    if (meters < 1.5) {
      Serial.println("Alarm!");
      digitalWrite(ALARM_LED_PIN, HIGH);
    } else {
      digitalWrite(ALARM_LED_PIN, LOW);
    }
  }
  delay(1000);
}
