

# Materiel

* Pro Mini ATMEGA328P 5V

	<https://www.aliexpress.com/item/Pro-Mini-ATMEGA328P-5V-16MHZ-for-Arduino-DIY-Atmega-328-Mini-ATMEGA328-Controller-Board-Panel-Module/32658074613.html>

* 5pcs Distance Sensor HC-SR04 Ultrasonic Sensors Transducer Position Electronics Module Diy Kit 5V HC-SR04 Ultrasonic Sensors 

	<https://www.aliexpress.com/item/1PC-HC-SR04-Ultrasonic-Module-Distance-Measuring-Transducer-Sensor-for-Arduino/32441153491.html>

* Wireless Serial 4 Pin RF Transceiver Bluetooth Module HC-06 3.6V-6V  

	<https://www.aliexpress.com/item/Wireless-Serial-4-Pin-RF-Transceiver-Bluetooth-Module-HC-06-3-6V-6V-4pin-Slave-for/32322563230.html>
